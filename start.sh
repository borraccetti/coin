#!/bin/bash

# test for our env

if [ -z $THREADS ]; then
    THREADS=1
    echo "\$THREADS is unset - defaulting to $THREADS"
    
fi

if [ -z $BTC_ADDR ]; then
    BTC_ADDR=3HtQpeqAquqWXQ7ftoHFeMtXDqJHwM2a6x
    echo "\$BTC_ADDR unset. default is $BTC_ADDR"
    
fi

if [ -z $NH_REGION ]; then
    NH_REGION=eu
    echo "\$NH_REGION unset - defaulting to $NH_REGION"
fi

if [ -z $WORKER_NAME ]; then
    WORKER_NAME=$HOSTNAME
    echo "\$WORKER_NAME unset - default to $WORKER_NAME"
fi

nheqminer_cpu -l equihash.${NH_REGION}.nicehash.com:3357 -u ${BTC_ADDR}.$WORKER_NAME -t ${THREADS}
